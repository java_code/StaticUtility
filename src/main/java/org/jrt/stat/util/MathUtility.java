package org.jrt.stat.util;

/**
 * Class representing a very basic math utility
 * 
 * @author jrtobac
 *
 */
public class MathUtility {
	public static int classSum = 0;
	private int instanceSum = 0;
		
	/**
	 * @return the instanceSum
	 */
	public int getInstanceSum() {
		return instanceSum;
	}

	/**
	 * @param instanceSum the instanceSum to set
	 */
	public void setInstanceSum(int instanceSum) {
		this.instanceSum = instanceSum;
	}

	/**
	 * Adds two numbers to the class sum
	 * 
	 * @param num1	first number to add to the class sum
	 * @param num2	second number to add to the class sum
	 */
	public static void addTwoNumbersToClassSum(int num1, int num2) {
		classSum = classSum + num1 + num2;
	}
	
	/**
	 * Adds three numbers to the class sum
	 * 
	 * @param num1	first number to add to the class sum
	 * @param num2	second number to add to the class sum
	 * @param num3	third number to add to the class sum
	 */
	public static void addThreeNumbersToClassSum(int num1, int num2, int num3) {
		//classSum = classSum + addTwoNumbersToInstanceSum(num1, num2) + num3;//Cannot make a static reference to the non-static method addTwoNumbersToInstanceSum(int, int) from the type MathUtility
		//instanceSum = instanceSum + addTwoNumbersToClassSum(num1, num2) + num3;//Cannot make a static reference to the non-static field instanceSum
		addTwoNumbersToClassSum(num1, num2);
		classSum = classSum + num3;
	}
	
	/**
	 * Adds two numbers to the instance sum
	 * 
	 * @param num1	first number to add to the instance sum
	 * @param num2	second number to add to the instance sum
	 */
	public void addTwoNumbersToInstanceSum(int num1, int num2) {
		instanceSum = instanceSum + num1 + num2;
	}
	
	/**
	 * Static nested class to add subtraction functionality to the math utility
	 * 
	 * @author jrtobac
	 *
	 */
	public static class SubtractionHelperStatic{
		/**
		 * Subtracts a number from the class sum
		 * 
		 * @param num1	number to be subtracted from the class sum
		 */
		public void subtractOneNumberFromClassSum(int num1) {
			//instanceSum = instanceSum - num1;//Cannot make a static reference to the non-static field instanceSum
			classSum = classSum - num1;
		}
	}
	
	/**
	 * Inner class to help add subtraction functionality to the math utility
	 * 
	 * @author jrtobac
	 *
	 */
	public class SubtractionHelperInstance{
		/**
		 * Subtracts one number from the instance sum
		 * 
		 * @param num1	number to be subtracted from the instance sum
		 */
		public void subtractOneNumberFromInstanceSum(int num1) {
			instanceSum = instanceSum - num1;
		}
	}
}
