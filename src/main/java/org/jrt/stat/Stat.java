package org.jrt.stat;

import org.jrt.stat.util.MathUtility;
/**
 * Main class to demonstrate static keyword in Java
 * 
 * @author jrtobac
 *
 */
public class Stat {

	public static void main(String[] args) {
		int num1 = 1;
		int num2 = 2;
		int num3 = 3;
		
		System.out.println("The class sum is: " + MathUtility.classSum);
		System.out.println("Adding " + num1 + " + " + num2 + " + " + num3 + " to classSum");
		MathUtility.addThreeNumbersToClassSum(num1, num2, num3);
		System.out.println("The class sum is: " + MathUtility.classSum);
		System.out.println();
		
		MathUtility mu = new MathUtility();
		System.out.println("The instance sum is: " + mu.getInstanceSum());
		System.out.println("Adding " + num1 + " + " + num2 + " to intanceSum");
		mu.addTwoNumbersToInstanceSum(num1, num2);
		System.out.println("The class sum is: " + MathUtility.classSum);
		System.out.println("The instance sum is: " + mu.getInstanceSum());
		System.out.println();
		
		MathUtility.SubtractionHelperStatic muStat = new MathUtility.SubtractionHelperStatic();
		System.out.println("Subtracting " + num1 + " from classSum");
		muStat.subtractOneNumberFromClassSum(num1);
		System.out.println("The class sum is: " + MathUtility.classSum);
		System.out.println("The instance sum is: " + mu.getInstanceSum());
		System.out.println();
		
		MathUtility.SubtractionHelperInstance muInst = mu.new SubtractionHelperInstance();
		System.out.println("Subtracting " + num2 + " from instanceSum");
		muInst.subtractOneNumberFromInstanceSum(num2);
		System.out.println("The class sum is: " + MathUtility.classSum);
		System.out.println("The instance sum is: " + mu.getInstanceSum());
	}
}
