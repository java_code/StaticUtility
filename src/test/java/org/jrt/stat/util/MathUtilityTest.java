package org.jrt.stat.util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * Class to test the MathUtility class and its nested and inner classes
 * @author jrtobac
 *
 */
class MathUtilityTest {

	/**
	 * Test to ensure two numbers are properly added to the class sum
	 */
	@Test
	void testAddTwoNumbersToClassSum_CorrectlyAddsNumbers() {
		MathUtility.classSum = 0;
		int expected = 3;
		int num1 = 1;
		int num2 = 2;
		
		MathUtility.addTwoNumbersToClassSum(num1, num2);
		assertEquals(expected, MathUtility.classSum);
	}
	
	/**
	 * Test to ensure three numbers are properly added to the class sum
	 */
	@Test
	void testAddThreeNumbersToClassSum_CorrectlyAddsNumbers() {
		MathUtility.classSum = 0;
		int expected = 6;
		int num1 = 1;
		int num2 = 2;
		int num3=3;
		
		MathUtility.addThreeNumbersToClassSum(num1, num2, num3);
		assertEquals(expected, MathUtility.classSum);
	}
	
	/**
	 * Test to ensure two numbers are properly added to the instance sum
	 */
	@Test
	void testAddTwoNumbersToInstanceSum_CorrectlyAddsNumbers() {
		MathUtility mu = new MathUtility();
		int expected = 3;
		int num1 = 1;
		int num2 = 2;
		
		mu.addTwoNumbersToInstanceSum(num1, num2);
		assertEquals(expected, mu.getInstanceSum());
	}
	
	/**
	 * Tests that that static nested class correctly subtracts a number from the class sum
	 */
	@Test
	void testSubtractOneNumberFromClassSum_correctlySubtractsNumber() {
		MathUtility.classSum = 4;
		int expected = 3;
		int num1 = 1;
		
		MathUtility.SubtractionHelperStatic mush = new MathUtility.SubtractionHelperStatic();
		mush.subtractOneNumberFromClassSum(num1);
		assertEquals(expected, MathUtility.classSum);
	}
	
	/**
	 * Tests that the non-static inner class correctly subtracts a number from the instance sum
	 */
	@Test  void testSubtractOneNumberFromInstanceSum_correctlySubtractsNumber() {
		int num1 = 1;
		int num2 = 2;
		int expected = 2;
		MathUtility mu = new MathUtility();
		mu.addTwoNumbersToInstanceSum(num1, num2);
		MathUtility.SubtractionHelperInstance mushi = mu.new SubtractionHelperInstance();
		mushi.subtractOneNumberFromInstanceSum(num1);
		assertEquals(expected, mu.getInstanceSum());
	}

}
